import React from 'react';
import { StatusBar } from 'react-native';

import styled from 'styled-components/native';

/* Redux */
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import reducers from '@reducers/reducers'; //variaveis globais definidas aqui (combineReducers)

/* Navigation */
import { createStackNavigator, createAppContainer } from 'react-navigation';
import StackIntroRoute from './src/config/routes';

/* init store redux */
const middlewares = [thunk];

if (__DEV__) {
  middlewares.push(logger);
}

const store = createStore(
  reducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(...middlewares)
);

const App = () => {
  const MainNavigation = createAppContainer(
    createStackNavigator(StackIntroRoute, {
      initialRouteName: 'Login',
      mode: Platform.OS === 'ios' ? 'modal' : 'card',
      headerMode: 'none'
    })
  );

  return (
    <Provider store={store}>
      <StatusBar barStyle="dark-content" />
      <SafeAreaViewStyled>
        <MainNavigation />
      </SafeAreaViewStyled>
    </Provider>
  );
};

const SafeAreaViewStyled = styled.SafeAreaView`
  flex: 1;
  justify-content: center;
`;

export default App;
