import React from 'react';
import { withNavigation } from 'react-navigation';
import { TouchableOpacity, View } from 'react-native';
import styled from 'styled-components/native';

const Company = props => {
  const { enterprise_name, enterprise_type, country, city, id, navigation } = props;

  return (
    <TouchableOpacity onPress={() => navigation.navigate('Details', { id })}>
      <Container>
        <View>
          <Description>{enterprise_name + ' (' + enterprise_type.enterprise_type_name + ')'}</Description>
          <Location>{country + ' - ' + city}</Location>
        </View>
      </Container>
    </TouchableOpacity>
  );
};

export default withNavigation(Company);

const Container = styled.View`
  padding-top: 10;
  padding-bottom: 10;
  flex-direction: row;
  border-bottom-width: 1;
  border-color: #aaa;
`;

const Description = styled.Text`
  font-size: 15;
`;

const Location = styled.Text`
  font-size: 12;
`;
