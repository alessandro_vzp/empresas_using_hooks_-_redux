import React, { useState } from 'react';
import { Modal, Alert, TouchableWithoutFeedback, TouchableOpacity } from 'react-native';
import styled from 'styled-components/native';

const FilterModal = props => {
  const [name_filter, setNameFilter] = useState('');
  const [type_filter, setTypeFilter] = useState('');

  const { isVisible, onFilter, closeModal } = props;

  const filter = () => {
    const data = name_filter && type_filter ? { name_filter, type_filter } : null;
    onFilter(data);
  };

  return (
    <Modal onRequestClose={closeModal} visible={isVisible} animationType="slide" transparent={true}>
      <TouchableWithoutFeedback onPress={closeModal}>
        <OffSet />
      </TouchableWithoutFeedback>
      <Container>
        <Header>Opções de filtro</Header>

        <Input
          placeholder="Nome da empresa"
          value={name_filter}
          onChangeText={value => setNameFilter(value)}
          autoCapitalize={'none'}
        />

        <Input
          placeholder="ID do tipo da empresa"
          value={type_filter}
          onChangeText={value => setTypeFilter(value)}
          autoCapitalize={'none'}
        />

        <ButtonsBar>
          <TouchableOpacity onPress={closeModal}>
            <Button>Cancelar</Button>
          </TouchableOpacity>
          <TouchableOpacity onPress={filter}>
            <Button>Filtrar</Button>
          </TouchableOpacity>
        </ButtonsBar>
      </Container>
      <TouchableWithoutFeedback onPress={closeModal}>
        <OffSet />
      </TouchableWithoutFeedback>
    </Modal>
  );
};

const Container = styled.View`
  background-color: white;
  justify-content: space-between;
`;

const OffSet = styled.View`
  flex: 1;
  background-color: rgba(0, 0, 0, 0.7);
`;

const Button = styled.Text`
  margin-left: 20;
  margin-top: 20;
  margin-bottom: 20;
  margin-right: 30;
`;

const Header = styled.Text`
  text-align: center;
  padding-left: 15;
  padding-right: 15;
  padding-top: 15;
  padding-bottom: 15;
  font-size: 15;
`;

const ButtonsBar = styled.View`
  flex-direction: row;
  justify-content: flex-end;
`;

const Input = styled.TextInput`
  width: 90%;
  height: 40;
  margin-top: 10;
  margin-left: 10;
  background-color: white;
  border-width: 1;
  border-color: #e3e3e3;
  border-radius: 6;
`;

export default FilterModal;
