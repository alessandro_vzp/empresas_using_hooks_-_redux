import React from 'react';
import { TouchableOpacity, ScrollView } from 'react-native';
import { withNavigation } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import styled from 'styled-components/native';
import backgroundName from '../../../assets/imgs/background_name.jpg';

const Details = ({ company, navigation }) => {
  const renderItem = (title, desc) => (
    <Info>
      <InfoHeader>{title}</InfoHeader>
      <Content>{desc}</Content>
    </Info>
  );

  return (
    <Container>
      <Background source={backgroundName}>
        <DarkMask>
          <IconBar>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Icon name={'arrow-circle-left'} size={26} color={'white'} />
            </TouchableOpacity>
          </IconBar>
          <TitleBar>
            <Title>{company.enterprise_name}</Title>
          </TitleBar>
        </DarkMask>
      </Background>

      <ContainerDetails>
        <ScrollView>
          {company.enterprise_type && renderItem('Type', company.enterprise_type.enterprise_type_name)}
          {renderItem('Description', company.description)}
          {renderItem('City', company.city)}
          {renderItem('Country', company.country)}
        </ScrollView>
      </ContainerDetails>
    </Container>
  );
};

const Container = styled.View`
  flex: 1;
`;

const Background = styled.ImageBackground`
  flex: 3;
`;

const ContainerDetails = styled.View`
  flex: 7;
  justify-content: center;
`;

const DarkMask = styled.View`
  flex: 1;
  background-color: rgba(0, 0, 0, 0.6);
`;

const Info = styled.View`
  width: 100%;
`;

const InfoHeader = styled.Text`
  color: white;
  background-color: #707070;
  padding-left: 10;
  font-size: 20;
`;

const Content = styled.Text`
  color: #303030;
  padding-left: 10;
  padding-right: 10;
  font-size: 17;
`;

const TitleBar = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

const Title = styled.Text`
  color: white;
  font-size: 45;
  margin-top: -10;
`;

const IconBar = styled.View`
  margin-top: 10;
  margin-left: 20;
  margin-right: 20;
  flex-direction: row;
  justify-content: space-between;
`;

export default withNavigation(Details);
