import React, { useState } from 'react';
import styled from 'styled-components/native';
import { withNavigation } from 'react-navigation';
import AuthInput from '../../core/AuthInput';
import backgroundImage from '../../../assets/imgs/login.jpg';

const Login = props => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const { signIn } = props;

  const validations = [];

  validations.push(email && email.includes('@'));
  validations.push(password && password.length >= 6);

  const validForm = validations.reduce((all, v) => all && v);

  const _signIn = () => {
    signIn(email, password);
  };

  return (
    <Background source={backgroundImage}>
      <Title>Empresas</Title>
      <FormContainer>
        <SubTitle>Informe seus dados</SubTitle>
        <Input icon="at" placeholder="E-mail" value={email} onChangeText={value => setEmail(value)} />
        <Input
          icon="lock"
          secureTextEntry={true}
          placeholder="Senha"
          value={password}
          onChangeText={value => setPassword(value)}
        />
        <Button disabled={!validForm} onPress={_signIn}>
          <ButtonText>Entrar</ButtonText>
        </Button>
      </FormContainer>
    </Background>
  );
};

const Input = styled(AuthInput)`
  margin-top: 5;
  background-color: #fff;
`;

const Background = styled.ImageBackground`
  flex: 1;
  width: 100%;
  align-items: center;
  justify-content: center;
`;

const Title = styled.Text`
  color: #fff;
  font-size: 40;
  margin-bottom: 10;
`;

const FormContainer = styled.View`
  background-color: rgba(0, 0, 0, 0.8);
  padding-top: 20;
  padding-left: 20;
  padding-right: 20;
  padding-bottom: 20;
  width: 90%;
`;

const SubTitle = styled.Text`
  color: #fff;
  font-size: 20;
`;

const Button = styled.TouchableOpacity`
  background-color: ${({ disabled }) => (disabled ? '#aaa' : '#080')};
  border-radius: 5;
  margin-top: 20;
  padding-top: 10;
  padding-left: 10;
  padding-right: 10;
  padding-bottom: 10;
  align-items: center;
`;

const ButtonText = styled.Text`
  color: #fff;
  font-size: 20;
`;

export default withNavigation(Login);
