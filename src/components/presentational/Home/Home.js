import React from 'react';
import { FlatList, TouchableOpacity } from 'react-native';
import styled from 'styled-components/native';
import Icon from 'react-native-vector-icons/FontAwesome';
import background_logo from '../../../assets/imgs/logo_ioasys.png';
import Company from '../../core/Company';
import FilterModal from '../FilterModal';

const Home = props => {
  const {
    showCleanFilter,
    showFilterCompanies,
    setShowFilterCompanies,
    loadCompanies,
    filterCompanies,
    companies
  } = props;

  return (
    <Container>
      <FilterModal
        isVisible={showFilterCompanies}
        onFilter={filterCompanies}
        closeModal={() => setShowFilterCompanies(false)}
      />
      <Background source={background_logo}>
        <DarkMask>
          <IconBar>
            <TouchableOpacity onPress={loadCompanies}>
              {showCleanFilter && <CleanFilter>Limpar Filtro</CleanFilter>}
            </TouchableOpacity>
            <TouchableOpacity onPress={() => setShowFilterCompanies(true)}>
              <Icon name={'eye'} size={26} />
            </TouchableOpacity>
          </IconBar>
        </DarkMask>
      </Background>
      <TaskContainer>
        <FlatList
          data={companies}
          keyExtractor={item => `${item.id}`}
          renderItem={({ item }) => <Company {...item} />}
        />
      </TaskContainer>
    </Container>
  );
};

const Container = styled.View`
  flex: 1;
`;

const Background = styled.ImageBackground`
  flex: 3;
`;

const DarkMask = styled.View`
  flex: 1;
  background-color: rgba(0, 0, 0, 0.6);
`;

const TaskContainer = styled.View`
  padding-left: 10;
  flex: 7;
`;

const CleanFilter = styled.Text`
  font-size: 20;
`;

const IconBar = styled.View`
  margin-top: 10;
  margin-left: 20;
  margin-right: 20;
  flex-direction: row;
  justify-content: space-between;
`;

export default Home;
