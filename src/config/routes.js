import React from 'react';
/* Screen imports */
import LoginScreen from '../screens/LoginScreen';
import HomeScreen from '../screens/HomeScreen';
import DetailsScreen from '../screens/DetailsScreen';

const routes = {
  Login: LoginScreen,
  Home: HomeScreen,
  Details: DetailsScreen
};

export default routes;
