import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { withNavigation } from 'react-navigation';
import { loadCompanyDetails } from '../useCases/detailsUseCase';
import Details from '../components/presentational/Details';

const DetailsScreen = props => {
  const id = props.navigation.getParam('id', '0');
  const company = useSelector(state => state.company.company);
  const dispatch = useDispatch();

  const _onLoadStart = () => {
    dispatch(loadCompanyDetails(id));
  };

  useEffect(_onLoadStart, []);

  return <Details company={company} />;
};

export default withNavigation(DetailsScreen);
