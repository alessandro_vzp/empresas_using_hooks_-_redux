import React from 'react';
import { useDispatch } from 'react-redux';
import { loginUser } from '../useCases/loginUseCase';
import Login from '../components/presentational/Login';

const LoginScreen = props => {
  const dispatch = useDispatch();
  const _onPressLogin = (email, password) => {
    dispatch(loginUser(email, password, () => props.navigation.navigate('Home')));
  };

  return <Login signIn={_onPressLogin} />;
};

export default LoginScreen;
