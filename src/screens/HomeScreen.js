import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { loadCompanies } from '../useCases/homeUseCase';
import Home from '../components/presentational/Home';

const HomeScreen = () => {
  const [showCleanFilter, setShowCleanFilter] = useState(false);
  const [showFilterCompanies, setShowFilterCompanies] = useState(false);
  const companies = useSelector(state => state.companies.companies);
  const dispatch = useDispatch();

  const _onLoadStart = () => {
    dispatch(loadCompanies());
    setShowCleanFilter(false);
  };

  const _onFilterStart = filters => {
    setShowCleanFilter(true);
    dispatch(loadCompanies(filters));
    setShowFilterCompanies(false);
  };

  useEffect(_onLoadStart, []);

  return (
    <Home
      loadCompanies={_onLoadStart}
      filterCompanies={_onFilterStart}
      companies={companies}
      showCleanFilter={showCleanFilter}
      showFilterCompanies={showFilterCompanies}
      setShowFilterCompanies={setShowFilterCompanies}
    />
  );
};

export default HomeScreen;
