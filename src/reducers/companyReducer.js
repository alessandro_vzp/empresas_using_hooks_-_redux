import { LOADING_COMPANY, COMPANY_LOADED } from '../actions/actions';

const initialState = {
  company: {},
  isLoading: false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case LOADING_COMPANY:
      return {
        ...state,
        isLoading: true
      };
    case COMPANY_LOADED:
      return {
        ...state,
        company: action.payload,
        isLoading: false
      };
    default:
      return state;
  }
};

export default reducer;
