import { combineReducers } from 'redux';

/* Reducers */
import userReducer from './userReducer';
import companiesReducer from './companiesReducer';
import companyReducer from './companyReducer';

export default combineReducers({
  user: userReducer,
  companies: companiesReducer,
  company: companyReducer
});
