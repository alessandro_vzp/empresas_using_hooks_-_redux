import { COMPANIES_LOADED, LOADING_COMPANIES } from '../actions/actions';

const initialState = {
  companies: [],
  isLoading: false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case LOADING_COMPANIES:
      return {
        ...state,
        isLoading: true
      };
    case COMPANIES_LOADED:
      return {
        ...state,
        companies: action.payload,
        isLoading: false
      };
    default:
      return state;
  }
};

export default reducer;
