import { USER_LOGGED_IN, LOADING_USER, USER_LOADED } from '../actions/actions';

const initialState = {
  name: null,
  email: null,
  isLoading: false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_LOGGED_IN:
      return {
        ...state,
        name: action.payload.investor_name,
        email: action.payload.email
      };
    case LOADING_USER:
      return {
        ...state,
        isLoading: true
      };
    case USER_LOADED:
      return {
        ...state,
        isLoading: false
      };
    default:
      return state;
  }
};

export default reducer;
