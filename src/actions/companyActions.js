import { COMPANY_LOADED, LOADING_COMPANY } from './actions';

export const companyLoaded = company => {
  return {
    type: COMPANY_LOADED,
    payload: company
  };
};

export const loadingCompany = () => {
  return {
    type: LOADING_COMPANY
  };
};
