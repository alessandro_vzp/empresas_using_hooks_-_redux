import { USER_LOGGED_IN, LOADING_USER, USER_LOADED } from './actions';

export const userLogged = user => {
  return {
    type: USER_LOGGED_IN,
    payload: user
  };
};

export const loadingUser = () => {
  return {
    type: LOADING_USER
  };
};

export const userLoaded = () => {
  return {
    type: USER_LOADED
  };
};
