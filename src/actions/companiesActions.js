import { COMPANIES_LOADED, LOADING_COMPANIES } from './actions';

export const companiesLoaded = companies => {
  return {
    type: COMPANIES_LOADED,
    payload: companies
  };
};

export const loadingCompanies = () => {
  return {
    type: LOADING_COMPANIES
  };
};
