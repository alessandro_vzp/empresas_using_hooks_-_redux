import CONSTANTS from '../config/constants';
import { userLogged, loadingUser, userLoaded } from '@actions/userActions';
import axios from 'axios';

export const loginUser = (email, password, successCallback = () => {}) => {
  return dispatch => {
    dispatch(loadingUser());
    axios
      .post(`${CONSTANTS.SERVER}/api/v1/users/auth/sign_in`, {
        email: email,
        password: password
      })
      .then(res => {
        axios.defaults.headers.common['access-token'] = `${res.headers['access-token']}`;
        axios.defaults.headers.common['client'] = `${res.headers['client']}`;
        axios.defaults.headers.common['uid'] = `${res.headers['uid']}`;

        dispatch(userLogged(res.data.investor));
        dispatch(userLoaded());

        successCallback();
      })
      .catch(err => console.log(err));
  };
};
