import CONSTANTS from '../config/constants';
import { loadingCompanies, companiesLoaded } from '@actions/companiesActions';

/* Axios */
import axios from 'axios';

export const loadCompanies = (filters = {}) => {
  let params = null;
  if (filters.name_filter && filters.type_filter) {
    params = {
      enterprise_types: filters.type_filter,
      name: filters.name_filter
    };
  }
  return dispatch => {
    dispatch(loadingCompanies());
    axios
      .get(`${CONSTANTS.SERVER}/api/v1/enterprises`, {
        params: params
      })
      .then(res => {
        dispatch(companiesLoaded(res.data.enterprises));
      })
      .catch(err => console.log(err));
  };
};
