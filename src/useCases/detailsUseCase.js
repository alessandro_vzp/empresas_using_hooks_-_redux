import { loadingCompany, companyLoaded } from '@actions/companyActions';
import CONSTANTS from '../config/constants';

import axios from 'axios';

export const loadCompanyDetails = id => {
  return dispatch => {
    dispatch(loadingCompany());
    axios
      .get(`${CONSTANTS.SERVER}/api/v1/enterprises/${id}`)
      .then(res => {
        dispatch(companyLoaded(res.data.enterprise));
      })
      .catch(err => console.log(err));
  };
};
